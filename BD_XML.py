#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  BD_XML.py
#
#  Copyright 2014 wolfang Torres <wolfang.Torres@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

import mysql.connector as MYDB
import time, datetime, copy, xml.etree.ElementTree as ET
from re import search

tipos_variables = {
    'int':'^(small|medium|big)?int(eger)?(\((\d+)\))?( unsigned)?( zerofill)?$',
    'bool':'^tinyint\(1\)$',
    'double':'^(double|real|float|decimal|numeric)(\((\d+)(,\d+)?\))?( unsigned)?( zerofill)?$',
    'date':'^date$',
    'time':'^time$',
    'datetime':'^datetime$',
    'timestamp':'^timestamp$',
    'varchar':'^(var)?char(\(\d+\))?$',
    'text':'^(tiny|medium|long)?(text|blob)( binary)?$',
    'set':'^(enum|set)\(.*\)$',
    }

convercion_variables = {
    'datetime':datetime.timedelta,
    'date':datetime.date,
    'time':datetime.time,
    'timestamp':time,
    'varchar':str,
    'str':str,
    'char':str,
    'text':str,
    'blob':str,
    'enum':str,
    'set':str,
    'int':int,
    'double':float,
    'float':float,
    'real':float,
    'bool':bool,
    }

estructura_time = {
    0:'año',
    1:'mes',
    2:'dia',
    3:'hora',
    4:'minuto',
    5:'segundo',
    6:'dia_semana',
    7:'dia_año',
    8:'hora_de_verano',
    }

mapeo_bd = {
        'mysql':MYDB,
        }


class Tabla():
    """Clase de ayuda para importar y exportar informacion entre python, mysql y xml"""


    class fila(dict):
        def __init__(self, padre, pos):
            #"padre" es el objeto 'Tabla' al que pertenece la fila y "pos" su posición de fila
            self.padre = padre
            self.pos = pos


        def __getitem__(self, llave):
            if type(llave) is str:
                return dict.__getitem__(self, llave)
            elif type(llave) is int:
                col = self.padre.columnas[llave]
                return dict.__getitem__(self, llave)
            else:
                raise IndexError('Solo se admiten llaves de tipo str o int, no {}'.format(type(llave)))
                
             
        def __setitem__(self, llave, valor):
            #sinplificar el espacio de nombre
            columnas = self.padre.columnas
            filas = self.padre.filas
            pos = self.pos

            #Verificar que no se modifico una columna que no existente
            if llave in columnas:
                #cambiar valor local
                dict.__setitem__(self, llave, valor)

                #Borrar fila modificada
                filas.remove(filas[pos])

                #Insertar nueva fila con el valor actualisado
                filas.insert(pos, tuple([self[col] for col in columnas]))

            else:
                raise TypeError('''Solo se pueden modificar columnas existentes, use "agregar_columna"
 para agregar una columna nueva''')

        def __delitem__(self, llave):
            #sinplificar el espacio de nombre
            columnas = self.padre.columnas
            filas = self.padre.filas
            defecto = self.padre._col[columnas.index(llave)]['defecto']
            pos = self.pos

            #Verificar que no se modifico una columna que no existente
            if llave in columnas:
                #cambiar valor local
                dict.__setitem__(self, llave, defecto)

                #Borrar fila modificada
                filas.remove(filas[pos])

                #Insertar nueva fila con el valor actualisado
                filas.insert(pos, tuple([self[col] for col in columnas]))

        def borrar(self):
            '''Borra la fila de la que proviene'''
            del self.padre[self.pos]


    def __init__(self, nombre='', coneccion=None):
        self.cnx = coneccion

        self.coneccion= {
        'user':'root',
        'password':'',
        'db':'',
        'host':'127.0.0.1',
        'port':'3306'
        }

        self.nombre = nombre

        self._col = []
        self._fil = []

        self.xml_texto = ''
        self.xml_declaration= '<?xml version="1.0" encoding="UTF-8"? standalone="yes"?>'
        self.xml_arbol = None

    @property
    def nombre(self):
        return self._nombre

    @nombre.setter
    def nombre(self, valor):
        if isinstance(valor, bytes): self._nombre = str(valor, 'utf-8')
        elif isinstance(valor, str): self._nombre = valor
        else:
            raise TypeError('el nombre debe ser un str o bytes')

    @property
    def filas(self):
        return self._fil

    @filas.setter
    def filas(self, valor):
        '''Las filas debe ser un contenedor con contenedores con igual cantidad de elementos que cantidad de columnas'''
        try:
            self._fil = [tuple(val[:len(self.columnas)]) for val in valor]
        except TypeError:
            self._fil = [tuple(valor[:len(self.columnas)])]

    @property
    def columnas(self):
        return [col['campo'] for col in self._col]

    @property
    def valores(self):
        l = []
        n = len(self.filas)

        for nfila in range(n):
            a = self.fila(self, nfila)
            p = 0
            for col in self.columnas:
                dict.__setitem__(a, col, self.filas[nfila][p])
                p += 1
            l.append(a)

        return tuple(l)

    def __getitem__(self, llave):
        if type(llave) is int:
            if llave > len(self.valores):
                fila = self.fila(self, llave)
                for col in self._col:
                    dict.__setitem__(fila, col['campo'], col['defecto'])
                return fila
            else:
                return self.valores[llave]
        elif type(llave) is slice:
            return self.valores[llave]
        else:
            raise TypeError('La llave debe ser int o slice')

    def __delitem__(self, llave):
        if type(llave) in (int, slice):
            del self.filas[llave]
        elif type(llave) is self.fila:
            del self.filas[llave.pos]
        else:
            raise TypeError('La llave debe ser int o slice o una fila de la tabla')

    def __setitem__(self, llave, valor):
        del self.filas[llave]
        self.__iadd__(valor, llave)

    def __add__(self, valor):
        tabla = self.copy()
        type(self).__iadd__(tabla, valor)
        return tabla

    def __radd__(self, valor):
        return self.__add__(valor)

    def __iadd__(self, valor, pos='final'):
        if pos == 'final': pos = len(self.filas)

        def obtener_fila(valor):
            fila = []
            for col in self._col:
                try:
                    fila.append(valor[col['campo']])

                except KeyError:
                    fila.append(col['defecto'])

                except TypeError:
                    try:
                        fila.append(valor[self._col.index(col)])
                    except IndexError:
                        fila.append(col['defecto'])
            return fila

        if type(valor) is type(self):
            filas = [obtener_fila(val) for val in valor]
            self.filas += filas
        else:
            filas = obtener_fila(valor)
            self.filas.insert(pos, filas)

        return self


    def __mul__(self, valor):
        tabla = self.copy()
        tabla.filas *= valor
        return tabla

    def __rmul__(self, valor):
        return self.__mul__(valor)

    def __imul__(self, valor):
        self.filas *= valor
        return self

    def __next__(self):
        if self.pos >= len(self.filas):
            raise StopIteration
        else:
            n = self.pos
            self.pos += 1
            return self.valores[n]

    def __iter__(self):
        self.pos = 0
        return self

    def __repr__(self):
        return '\n'.join(((self.nombre), repr(self._col), repr(self._fil)))

    def __str__(self):
        extra = 4

        n = 0
        t = 0
        max = 0
        for i in self.columnas:
            if len(i) > n: n = len(i)
            max += len(i)
            t += 1

        formato = ('{:'+str(n)+'}'+(' '*extra))*len(self.columnas)

        nombre = self.nombre.upper().center(max+(extra*t))

        valores = [col.upper().center(n) for col in self.columnas]

        cols = formato.format(*valores)

        sep = ((('-' * n)  + '-'*extra) * t)

        formato = ('{:'+str(n)+'}'+(' '*extra))*len(self.columnas)
        valores = []
        for fila in self._fil:
            v = []
            for valor in fila:
                if isinstance(valor,str): val = valor.ljust(n)
                elif isinstance(valor,bool): val = str(valor).center(n)
                elif isinstance(valor,int): val = str(valor).zfill(n)
                else: val = str(valor).center(n)
                v.append(val)
            valores.append(formato.format(*v))

        filas = '\n'.join(valores)

        return '\n'.join((sep, nombre, sep, cols, sep, filas, sep))

    def __len__(self):
        return len(self.filas)

    def index(self, val):
        try:
            n = self.filas.index(tuple(val))

        except IndexError:
            raise IndexError('''El tuple de fila no se encontro,
Si estas intetando indexar una fila obtenida atraves de fila = tabla[x],
Prueba fila.pos''')

        return n

    def copy(self, *slices):
        tabla = copy.deepcopy(self)
        if not slices: slices = (None,None,None)
        tabla.filas = tabla.filas.__getitem__(slice(*slices))
        return tabla

    def agregar_columna(self, **arg):
        '''Agrega una columa a la tabla con la información dada o la de por defecto'''
        campo = arg.get('campo', str())
        tipo = arg.get('tipo', 'varchar')
        nulo = arg.get('nulo', 'YES')
        llave = arg.get('llave', str())
        defecto = arg.get('defecto', None)
        extra = arg.get('extra', str())
        self._col.append({'campo':campo,'tipo':tipo,'nulo':nulo,'llave':llave,'defecto':defecto,'extra':extra})
        return True

    def eliminar_columna(self, campo):
        '''Elimina una columna por nombre'''

        for col in self._col:
            if col['campo'] == campo:
                self._col.remove(col)

                return True
        return False

    def ver_columna(self, nombre_columna, *info):
        '''Regresa la información de una columna (tipo, nulo, llave, defecto, extra),
        Si se pide una caracteristica se regresa un valor, si se piden mas regresa un tuple,
        regresa None si no hay un acolumna'''
        for col in self._col:
            if col['campo'] == nombre_columna:
                if len(info) > 1:
                    i = [col[carac] for carac in info]

                else: i = col[info[0]]

                return i
        return None

    def importar_xml(self, xml, **kw):
        """importar_xml(xml, tipo=('texto'/'archivo')
        Crear los datos de la tabla a base de un archivo xml"""
        tipo = kw.get('tipo', 'texto')

        if tipo == 'texto':
            self.xml_texto = xml
            self.xml_tree = ET.XML(xml)
            root = self.xml_tree
        elif tipo == 'archivo':
            self.xml_tree = ET.parse(xml)
            root = self.xml_tree.getroot()
            self.xml_texto = ET.tostring(root, encoding='unicode')
        else:
            raise AttributeError('''Se debe indicar si el tipo es literal xml o la dir del archivo xml
(tipo='texto'/tipo='archivo')''')

        self.nombre = root.get('nombre', root.tag)

        #Llena la información de las columnas
        self._col = []
        for col in root.find('columnas'):
            campo = col.tag
            tipo = col.get('tipo')
            nulo = col.get('nulo')
            llave = col.get('llave')
            defecto = col.get('defecto')
            extra = col.get('extra')

            self._col.append({
            'campo':campo,
            'tipo':tipo,
            'nulo':nulo,
            'llave':llave,
            'defecto':defecto,
            'extra':extra,
            })

        #LLena la información de la filas
        fil = []

        for fila in root.findall('fila'):

            valores = []
            for n in range(len(self.columnas)):
                if self._col[n]['tipo'] in ('timestamp', 'datetime'):
                    valores.append(_cambiar_xml_a_time(fila[n]))
                else:
                    valores.append(self._cambiar_tipo_dato(fila[n].text, self._col[n]['tipo']))

            fil.append(valores)
        self.filas = fil
        return True

    def exportar_xml(self, metadatos=True):
        '''Exporta los datos de la tabla a texto xml'''
        self.xml = arbol = ET.Element('tabla', nombre=self.nombre)

        #Agrega la estructura de los Encabesados y la Metadata
        if metadatos:
            columnas = ET.SubElement(arbol,'columnas')
            for col in self._col:
                tipo = col['tipo']
                nulo = col['nulo']
                llave = col['llave']
                defecto = col['defecto']
                extra = col['extra']

                if tipo is None: tipo = str(None)
                if nulo  is None: nulo = str(None)
                if llave is None: llave = str(None)
                if defecto is None: defecto = str(None)
                if extra is None: extra = str(None)

                columna = ET.SubElement(columnas, col['campo'],
                tipo = tipo,
                nulo = nulo,
                llave = llave,
                defecto = defecto,
                extra = extra,
                )

        #Agrega los Datos
        for fil in self._fil:
            fila = ET.SubElement(arbol, "fila")
            for col in self._col:
                val = fil[self._col.index(col)]
                if isinstance(val, time.struct_time):
                    fila.append(_cambiar_time_a_xml(val, col['campo']))
                else:
                    columna = ET.SubElement(fila, col['campo'])
                    columna.text = str(val)

        #Escribe el xml parceado en texto y en el objeto
        self.xml_arbol = ET.ElementTree(arbol)
        self.xml_texto = ET.tostring(arbol, encoding='unicode')
        return self.xml_texto

    def escribir_xml(self, ubicacion):
        '''Escribe los datos xml a un archivo'''
        archivo = open(ubicacion, mode='w')
        self.xml_arbol.write(archivo, encoding='unicode')

    def obtener_datos(self, con=None, **args):
        '''Importa los datos para conectarse a la base de datos o un dic bien estructurado,
        http://dev.mysql.com/doc/connector-python/en/connector-python-connectargs.html '''
        if not con is None: self.coneccion = con
        else:
            self.coneccion['user'] = args.get('usuario', self.coneccion['user'])
            self.coneccion['password'] = args.get('contraseña', self.coneccion['password'])
            self.coneccion['db'] = args.get('bd', self.coneccion['db'])
            self.coneccion['host']  = args.get('servidor', self.coneccion['host'])
            self.coneccion['port'] = args.get('puerto', self.coneccion['port'])

    def seleccionar_bd(self, tabla=None, cols=('*',), condicion='', cantidad=100, bd='mysql'):
        '''Importa los datos de la tabla desde la base de datos'''
        if bool(tabla): self.nombre = tabla
        BD = self.mapeo_bd[bd]
        self.cnx = BD.connect(**self.coneccion)
        self.cur = self.cnx.cursor(False, True)
        self._col_bd(cols)
        self._fila_bd(condicion, cantidad)

        numero_fil = self.cur.rowcount
        self.importar()
        self.cnx.close()
        return numero_fil

    def insertar_bd(self, nombre_tabla=None, columnas=None, bd='mysql'):
        '''Exporta los datos de la tabla a la base de datos, regresa getlasrowid() por conveniencia'''

        if nombre_tabla is None:
            nombre_tabla = self.nombre
        if columnas is None:
            columnas = self.columnas
        columnas = str(tuple(columnas)).replace("'","`")

        self.exportar()

        if not self.cnx:
            BD = self.mapeo_bd[bd]
            self.cnx = BD.connect(**self.coneccion)
        self.cur = cur = self.cnx.cursor()

        datos = {'tabla':nombre_tabla, 'columnas':columnas, }
        for fil in self._fil:
            fil = tuple(fil)
            datos['valores']=fil
            print("INSERT INTO `{tabla}` {columnas} VALUES {valores}".format(**datos))
            cur.execute("INSERT INTO `{tabla}` {columnas} VALUES {valores}".format(**datos))

        id = cur.getlastrowid()
        cnx.commit()
        self.cnx.close()
        return id

    def actualisar_bd(self, tabla=None, columnas=None, columnas_llaves=[], bd='mysql'):
        '''Realisa un UPDATE de todas la columnas usando automaticamente las llaves PRI para la clausula WHERE,
        las llaves no tienen que estar dentro de la columnas a actualisar,
        se pueden usar columnas propias como llaves a responsabilidad del usuario,
        regresa ROW_COUNT() por conveniencia'''

        BD = self.mapeo_bd[bd]

        if tabla is None:
            tabla = self.nombre
        if columnas is None:
            columnas = self.columnas

        datos = {'tabla':tabla, }

        #Busca las llaves PRI
        llaves = [i['campo'] for i in self._col if (i['llave'] == 'PRI')]
        llaves = llaves + list(columnas_llaves)
        assert llaves, "La tabla no tiene ninguna columna PRI o columna que usar como llave"

        if not self.cnx:
            BD = self.mapeo_bd[bd]
            self.cnx = BD.connect(**self.coneccion)
        self.cur = cur = self.cnx.cursor()

        for val in self.valores:
            #Prepara str con los datos a actualisar
            valores = {}
            for col in columnas:
                valores['`{}`'.format(col)]=val[col]
            valores = str(valores).replace("'","").replace(":","=")[1:-1]
            datos['valores'] = valores

            #Prepara las clausulas WHERE
            datos['condicion'] = _crear_condicion_de_llaves(llaves, val)

            print("UPDATE `{tabla}` SET {valores} WHERE {condicion}".format(**datos))
            cur.execute("UPDATE `{tabla}` SET {valores} WHERE {condicion}".format(**datos))

        numero_fil = cur.rowcount
        cnx.commit()
        cnx.close()
        return numero_fil

    def borrar_bd(self, tabla=None, columnas_llaves=[], bd='mysql'):
        '''Realisa un DELETE usando automaticamente las llaves PRI para la clausula WHERE,
        se pueden usar columnas propias como llaves a responsabilidad del usuario,
        regresa ROW_COUNT() por conveniencia'''
        BD = self.mapeo_bd[bd]

        if tabla is None:
            tabla = self.nombre

        datos = {'tabla':tabla, }

        #Busca las llaves PRI
        llaves = [i['campo'] for i in self._col if (i['llave'] == 'PRI')]
        llaves = llaves + list(columnas_llaves)
        assert llaves, "La tabla no tiene ninguna columna PRI o columna que usar como llave"

        if not self.cnx:
            BD = self.mapeo_bd[bd]
            self.cnx = BD.connect(**self.coneccion)
        self.cur = cur = self.cnx.cursor()

        for fila in self._fil:
            #Prepara las clausulas WHERE
            datos['condicion'] = _crear_condicion_de_llaves(llaves, val)

            cur.execute("DELETE FROM `{tabla}` WHERE {condicion}".format(**datos))

        numero_fil = cur.rowcount
        cnx.commit()
        cnx.close()
        return numero_fil

    def _col_bd(self, cols=None):
        meta = ('campo','tipo','nulo','llave','defecto','extra')
        self.cur.execute('SHOW COLUMNS FROM {}'.format(self.nombre))
        con = self.cur.fetchall()
        self._col = []
        for i in con:
            if (cols is None) or ('*' in cols and not str(i[0],'utf-8') in cols) or (str(i[0],'utf-8') in cols and not '*' in cols):
                col = {}
                for n in range(6):
                    try: atrib = str(i[n],'utf-8')
                    except TypeError: atrib = None
                    col[meta[n]] = atrib
                self._col.append(col)

    def _fila_bd(self, condicion, cantidad):
        col = [i['campo'] for i in self._col]
        col = str(tuple(col))[1:-1].replace("'","`")
        if col[-1] == ',': col = col[:-1]

        print("SELECT {} FROM {} {}".format(col, self.nombre, condicion))
        self.cur.execute("SELECT {} FROM {} {}".format(col, self.nombre, condicion))
        con = self.cur.fetchmany(cantidad)
        self._fil = con
        self._convertir_datos()

    def _convertir_datos(self):
        tipo = [i['tipo'] for i in self._col]
        t = []
        for fil in self._fil:
            n = 0
            f = []
            for val in fil:
                if val is None:
                    f.append(None)
                else: f.append(self._cambiar_tipo_dato(val,tipo[n]))
                n += 1
            t.append(tuple(f))
        self._fil = t

    def _cambiar_tipo_dato(self, valor, tipo):
        if isinstance(valor, bytes):
            valor = str(valor, 'utf-8')
        else:
            try:
                valor = str(valor)
            except Exception:
                raise AttributeError('Valor debe ser bytes o str')

        if valor == 'None':
            return None

        for p in tipos_variables:
            if search(tipos_variables[p], tipo):
                t = convercion_variables[p]
            else:
                try:
                    t = convercion_variables[tipo]
                except KeyError:
                    continue

            if t is str:
                return valor

            elif t is int:
                return int(valor,10)

            elif t is float:
                return float(valor)

            elif t is bool:
                return bool(int(valor))


            elif t is time:
                s = search('(\d\d\d\d)-(\d\d)-(\d\d) (\d\d):(\d\d):(\d\d)', valor)
                return time.struct_time([int(s.group(n)) for n in range(1,7)] + [0]*3)

            elif t is datetime.timedelta:
                s = search('(\d\d\d\d)-(\d\d)-(\d\d) (\d\d):(\d\d):(\d\d)', valor)
                A = int(s.group(1)) * 365
                M = int(s.group(2)) * 30
                D = int(s.group(3)) + M + A
                h = int(s.group(4)) * 3600
                m = int(s.group(5)) * 60
                s = int(s.group(6)) + h + m
                return datetime.timedelta(days=D, seconds=s)

            elif t is datetime.time:
                s = search('(\d\d):(\d\d):(\d\d)', valor)
                return datetime.time(hour=int(s.group(1)), minute=int(s.group(2)), second=int(s.group(3)))

            elif t is datetime.date:
                s = search('(\d\d\d\d)-(\d\d)-(\d\d)', valor)
                return time.struct_time([int(s.group(n)) for n in range(1,4)] + [0]*6)

            #Mas converciones a otros formatos

        return None

def _cambiar_time_a_xml(objeto_struct_time, tag):
    nodo = ET.Element(tag)

    for n in range(9):
        subnodo = ET.SubElement(nodo, estructura_time[n])
        subnodo.text = str(objeto_struct_time[n])

    return nodo

def _cambiar_xml_a_time(nodo):
    datos = [int(nodo[n].text) for n in range(9)]
    return time.struct_time(datos)

def _crear_condicion_de_llaves(llaves, valores):
    sep = 'AND'
    formato = '{a} = {b}'
    condicion = {}
    for llave in llaves:
        condicion['`{}`'.format(llave)] = valores[llave]
    items = tuple(condicion.items())
    clausulas =[]
    for a, b in items:
        texto = formato.format(**{'a':a, 'b':b})
        clausulas.append(texto)
        if items.index((a, b)) == len(items)-1: break
        clausulas.append(sep)

    return ' '.join(clausulas)


if __name__ == '__main__':
    ###Crear Tabla
    personas = p = Tabla('personas')

    #Asignar Nombre, se usa para identificar la tabla cuando exportando a una BD o XML
    nombre = p.nombre
    p.nombre = nombre

    ##Agregar columnas
    p.agregar_columna(campo='id', tipo='int', llave='pri')
    p.agregar_columna(campo='nombre', tipo='str', defecto="Jhon")
    p.agregar_columna(campo='apellido', tipo='str', defecto="Doe")

    ###Manipulación de Datos
    #Insertar
    print('Agregar los datos de diferentes formas')
    p += {'nombre':'wolfang','id':1,'apellido':'torres'}
    p = p + {'id':2, 'apellido':'torres', 'nombre':'wendy',}
    p = (3, 'carlos', 'molano') + p
    print(p)

    #Insertar una lina vacia llena los datos faltantes con el "defecto"
    print('insetar linea vacia')
    p += ()
    print(p)

    #Multiplicación
    print('Tambien se puede multiplicar')
    p *= 3
    print(p)

    #Copiado
    print('las tablas se pueden copiar, use slice en la funcion de copia( tabla[a:b:c] => copy(a,b,c) )')
    c = p.copy(None,2)
    c.nombre = 'clientes'
    print(c)

    d = p.copy()
    d.nombre = 'pacientes'
    d.filas = d.filas[2]
    print(d)

    #Tambien puedes sumar dos copias
    e = c + d
    del c
    del d
    e.nombre = 'copiado'
    print(e)

    #Modificación de una fila
    print('Modificar las filas de diferentes formas')
    p[1] = {'apellido':'molano', 'nombre':'carlos', 'id':3,}
    p[2] = [2, 'wendy', 'torres']
    print(p)

    #Modificar un registro
    print('Modificar los registros de diferentes formas')
    p[0]['id']=4
    p[1]['nombre']='juan'
    print(p)

    #Borrar
    print('Borrar las filas')
    del p[0]
    print(p)

    #Convertir al Defecto
    print('Borrar un registro lo convierte en el defecto')
    del p[1]['nombre']
    del p[1]['apellido']
    print(p)

    ###La tablas pueden ser operadas de diferentes formas
    ##Este es mas o menos el equivalente a:
    #UPDATE <tabla> SET nombre = CONCAT(nombre,' ',apellido), apellido = NULL WHERE id = 2
    print("UPDATE <tabla> SET nombre = CONCAT(nombre,' ',apellido), apellido = NULL WHERE id = 2")
    for fila in p:
        if fila['id'] == 2:
            fila['nombre'] += ' ' + fila['apellido']
            fila['apellido'] = None
    print(p)

    ##Este es mas o menos el equivalente a:
    #DELETE FROM <tabla> WHERE MOD(id,2) = 0 LIMIT 1
    print("DELETE FROM <tabla> WHERE MOD(id,2) = 0 LIMIT 1")
    n = 0
    nmax = 1
    for fila in p:
        if fila['id'] % 2 == 0:
            del p[fila]
            n += 1
            if n >= nmax: break
    print(p)


    ###Importacion y exportación xml
    #Crear Tabla
    print()
    print('Importacion y exportacion xml')

    dir = 'tabla_xml.xml'
    compras = Tabla("Compras")
    compras.agregar_columna(campo="nombre_proveedor", tipo="varchar")
    compras.agregar_columna(campo="codigo_proveedor", tipo="varchar")
    compras.agregar_columna(campo="fecha", tipo="timestamp")
    compras.agregar_columna(campo="codigo", tipo="varchar")
    compras.agregar_columna(campo="cantidad", tipo="int")
    compras.agregar_columna(campo="pn1", tipo="float")
    compras.agregar_columna(campo="pvp1", tipo="float")

    compras += {'nombre_proveedor':'wolfang',
    'codigo_proveedor':'v24404292',
    'fecha':time.localtime(),
    'codigo':'abc',
    'cantidad':None,
    'pn1':10.0,
    'pvp1':12.0,
    }
    print(compras)

    ##Exportación
    #Texto
    xml = compras.exportar_xml(True)
    #Archivo
    compras.escribir_xml(dir)

    ##Importación
    #Texto
    print('texto')
    f = Tabla()
    f.importar_xml(xml, tipo='texto')
    print(f)

    #Archivo
    print('archivo')
    f = Tabla()
    f.importar_xml(dir, tipo='archivo')
    print(f)

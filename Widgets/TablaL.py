"""
Simple table for dysplay information using ttk Treeview class.
"""
try:
    import Tkinter as tk
    import tkFont
except ImportError: # py3k
    import tkinter as tk
    import tkinter.font as tkFont
from tkinter import ttk

class TablaL(ttk.Frame):
    def __init__(self, master=None, **kw):
        self.altura = kw.pop('altura', 15)
        self._variable = kw.pop('variable', None)
        self._funcion = kw.pop('funcion', None)
        
        ttk.Frame.__init__(self, master, **kw)

        self._filas=[]        
        self.__colocar_widgets()
        self.__bind_evntos()

    def __colocar_widgets(self):
        self._arbol = ttk.Treeview(self, height=self.altura,
                                   show="headings", selectmode="extended")
        self._scroll = ttk.Scrollbar(self, orient="vertical",
                                     command=self._arbol.yview)
        self._arbol["yscrollcommand"]=self._scroll.set

        self._scroll.pack(side="right", fill="y")        
        self._arbol.pack(expand="yes", fill="both")

    def __bind_evntos(self):
        if not self._variable is None:
            v = lambda: self._variable.set(self._filas.index(self._arbol.focus()))
        else:
            v = lambda: "a"
        if not self._funcion is None:
            f = lambda: self._funcion(self._filas.index(self._arbol.focus()))
        else:
            f = lambda: "a"
        def a(event):
            v()
            f()
        self._arbol.tag_bind('fila', '<Double-ButtonPress-1>',a)

    def columnas(self, t, a):
        c=[]
        self._arbol['columns'] = t
        for i in range(len(t)):
            self._arbol.column(t[i], width=a[i], anchor='center')
            self._arbol.heading(t[i], text=t[i].upper())

    def escribir(self, valores=("a",)):
        for i in valores:
            self._filas.append(self._arbol.insert('', 'end' ,values=i
                                                  , tags="fila"))

    def borrar(self):
        for i in self._filas:
            self._arbol.delete(i)
        self._filas=[] 
        
    
def pr(val):
    print(val)
           
def prueva():
    import sys
    root = tk.Tk()
    fr=ttk.Frame(root)    
    fila = tk.StringVar()
    lab = ttk.Label(fr, textvariable=fila)
    tabla = TablaL(fr, funcion=pr, variable=fila, altura=20)
    tabla.pack(expand="yes", fill="both")
    fr.pack()
    lab.pack()
    t=("a", "b", "c", "d")
    a=(50, 30, 50, 30)
    tabla.columnas(t, a)

    a=[]
    for i in range(1, 101):
        a.append(("objeto",i,"faltan", 100-i))
    a=tuple(a)
    tabla.escribir(a)

    if 'win' not in sys.platform:
        style = ttk.Style()
        style.theme_use('clam')
        
    root.mainloop()    

if __name__ == '__main__':
    prueva()


        
